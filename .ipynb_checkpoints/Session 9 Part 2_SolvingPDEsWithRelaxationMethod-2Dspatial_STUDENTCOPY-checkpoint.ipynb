{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Session 9 Part 2: Relaxation Method to solve spatial PDEs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib import mlab\n",
    "\n",
    "import time\n",
    "from IPython import display"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "---\n",
    "\n",
    "# 2D Partial Differential Equations\n",
    "\n",
    "Many problems in Geosciences can be represented as\n",
    "    Partial Differential Equations (PDEs).\n",
    "\n",
    "For example, the time evolving advection-diffusion of pollution away from a chimney.\n",
    "\n",
    "![Air pollution](https://breakingenergy.com/wp-content/uploads/sites/2/2013/12/181753791.jpg)\n",
    "**Figure 1: Advection-diffusion of pollution away from a chimney.**\n",
    "\n",
    "\n",
    "- In some cases analytical solutions can be found.\n",
    "- In many cases there are no analytical solutions!\n",
    "- So we need to solve numerically.\n",
    "\n",
    "Other examples include:\n",
    "\n",
    "- Oceanic and atmospheric flow for weather forecasting and climate projection,\n",
    "- Ice-sheet dynamics,\n",
    "- Mantle Convection,\n",
    "- The movement of sea-ice in the ocean.\n",
    "\n",
    "\n",
    " Aim of this session is to outline some approaches to\n",
    "    numerically modelling simple PDEs.\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "from scipy import sparse\n",
    "import scipy.sparse.linalg as linalg   # linear algebra for sparse systems\n",
    "\n",
    "def createSecondOrderDiffMatrix_d2y_dx2(N, h):\n",
    "    M_SecondOrderTerm = ( -2*sparse.eye(N,N,0) + sparse.eye(N,N,1) + sparse.eye(N,N,-1) ) / h**2\n",
    "    return M_SecondOrderTerm\n",
    "    \n",
    "def createFirstOrderDiffMatrix_dy_dx(N, h):\n",
    "    M_FirstOrderTerm = ( sparse.eye(N,N,1) - sparse.eye(N,N,-1) ) / (2*h)\n",
    "    return M_FirstOrderTerm\n",
    "    \n",
    "def returnDiagonalMatrixFor_y(N):\n",
    "    M_forY = sparse.eye(N,N,0)\n",
    "    return M_forY"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 4 The Laplacian\n",
    "\n",
    "\n",
    "In this Session, we focus entirely on three problems which\n",
    "    include,\n",
    "\n",
    "- the Laplacian operator ($\\nabla^2 \\equiv \\nabla\\cdot\\nabla \\equiv \\Delta$) \n",
    "- and work in 1 & 2-D Cartesian co-ordinates.\n",
    "\n",
    "Many problems are best solved by working in co-ordinate systems that respect the symmetry of the problem. For example spherical co-ordinates are a natural co-ordinate system when working with the Earth.\n",
    "\n",
    "But we'll just focus on Cartesian co-ordinates where\n",
    "- The Laplacian in 2D Cartesian co-ordinates is $$\\nabla^2 =\n",
    "  \\frac{\\partial^2}{\\partial x^2}+\\frac{\\partial^2}{\\partial y^2}$$\n",
    "\n",
    "\n",
    "The first thing we need to do is represent $\\nabla^2$ in a discretised form, $\\nabla^2_h$. \n",
    "\n",
    "The subscript $h$ is just to show we have discretised across steps of size $h$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "---\n",
    "\n",
    "## Discretising the Laplacian in 2D\n",
    "\n",
    "In 2D, the Laplacian has 2 terms,\n",
    "\n",
    "$$\\nabla^2 = \\frac{\\partial^2}{\\partial x^2}+\\frac{\\partial^2}{\\partial y^2}$$\n",
    "\n",
    "which we can discretises into separate $x$ and $y$ terms,\n",
    "\n",
    "$$ \\nabla^2_h u(x,y)=\\frac{u(x+h,y)-2u(x,y)+u(x-h,y)}{h^2}+ \\frac{u(x,y+h)-2u(x,y)+u(x,y-h)}{h^2} $$\n",
    "\n",
    "In this example we have assumed that the spatial step sizes are the same in the $x$ and $y$ directions. i.e. $dx=dy=h$.\n",
    "\n",
    "By grouping like terms, this reduces to a form that can be put into a banded matrix:\n",
    "  \n",
    "$$ h^2 \\nabla^2_h u(x,y)=u(x+h,y)+u(x,y+h)-4u(x,y)+ u(x-h,y)+u(x,y-h) $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 4.1 EXAMPLE: Solving the Laplace Equation $\\nabla^2 \\phi = 0$ using the relaxation method\n",
    "\n",
    "$$ h^2 \\nabla^2_h u(x,y)=u(x+h,y)+u(x,y+h)-4u(x,y)+ u(x-h,y)+u(x,y-h) =0$$\n",
    "\n",
    "Since the RHS is zero, we can rearrange this to:\n",
    "\n",
    "$$u(x,y) = \\frac{1}{4} \\left( u(x+h,y)+u(x,y+h)-)+ u(x-h,y)+u(x,y-h) \\right)$$\n",
    "\n",
    "So Poisson's equation just means that each value is an average of the adjacent values.\n",
    "\n",
    "Consider the problem where we have a metal sheet where 3 sides are held at zero volts and the top edge is held at a finite voltange, $V$.\n",
    "\n",
    "In this problem, we know the voltages along all boundaries.\n",
    "\n",
    "Since we are looking for a steady state solution, we can find the final field by repeatedly averaging the values in adjacent cells, whilst holding the boundary values constant, until the resulting field is stable.\n",
    "\n",
    "<img src=\"http://www-personal.umich.edu/~mejn/cp/figures/fig9-2.png\" width=30%>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Constants\n",
    "M = 100         # Grid squares on a side\n",
    "V = 1.0         # Voltage at top wall\n",
    "target = 1e-6   # Target accuracy\n",
    "\n",
    "# Create arrays to hold potential values\n",
    "phi = np.zeros([M+1,M+1],float)\n",
    "phi[0,:] = V\n",
    "phiprime = np.empty([M+1,M+1],float)\n",
    "\n",
    "useAnimation = True\n",
    "\n",
    "# Main loop\n",
    "delta = 1.0\n",
    "n=0\n",
    "while delta>target:\n",
    "\n",
    "    # Calculate new values of the potential\n",
    "    phiprime = np.copy(phi)\n",
    "    phi[1:-1,1:-1] = 0.25*(phiprime[0:-2,1:-1]+phiprime[1:-1,0:-2]+phiprime[2:,1:-1]+phiprime[1:-1,2:])\n",
    "\n",
    "    # Calculate maximum difference from old values\n",
    "    delta = np.max( np.abs(phi-phiprime) )\n",
    "    n = n+1\n",
    "\n",
    "    if(useAnimation):\n",
    "        plt.imshow(phi, hold=False)\n",
    "        plt.title(\"Target \"+str(target)+ \"      Delta \"+str(delta))\n",
    "        display.display(plt.gcf())\n",
    "        display.clear_output(wait=True)\n",
    "        time.sleep(0.001) # necessary to get the plot up.\n",
    "\n",
    "\n",
    "    \n",
    "print(\"This took \", n, \"timesteps\")\n",
    "\n",
    "# Make a plot\n",
    "plt.imshow(phi)\n",
    "plt.colorbar()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 4.2 EXERCISE:  Solving Poisson's Equation $\\nabla^2 \\phi = -\\frac{\\rho}{\\epsilon_0}$ using the relaxation method\n",
    "\n",
    "Start with the disctretisation\n",
    "\n",
    "$$ \\nabla^2_h u(x,y)= \\frac{u(x+h,y)+u(x,y+h)-4u(x,y)+ u(x-h,y)+u(x,y-h)}{h^2} = -\\frac{\\rho(x,y)}{\\epsilon_0}$$\n",
    "\n",
    "modify the relaxation code for the Laplace equation so solve the problem where the are two square charges placed inside a 2D square box. The potential is zero on all the walls and the carge densities are +1Cm$^{-1}$ and -1Cm$^{-1}$.\n",
    "\n",
    "<img src=\"http://www-personal.umich.edu/~mejn/cp/figures/fig9-4.png\" width=30%>\n",
    "\n",
    "Work in units where $\\epsilon_0=1$ and iterate until the difference between successive updates is less than $10^{-6}$V everywhere.\n",
    "\n",
    "The solution should look like:\n",
    "\n",
    "<img src=\"http://www-personal.umich.edu/~mejn/cp/figures/fig9-5.png\" width=30%>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### MODIFY THE CODE BELOW\n",
    "\n",
    "# Constants\n",
    "M = 100         # Grid squares on a side\n",
    "target = 1e-6   # Target accuracy\n",
    "\n",
    "# Create arrays to hold potential values\n",
    "phi = np.zeros([M+1,M+1],float)\n",
    "phiprime = np.empty([M+1,M+1],float)\n",
    "\n",
    "# Main loop\n",
    "delta = 1.0\n",
    "n=0\n",
    "\n",
    "while delta>target:\n",
    "    # Calculate new values of the potential\n",
    "    phiprime = np.copy(phi)\n",
    "    phi[1:-1,1:-1] = ...\n",
    "    \n",
    "    # Since, parameter choices mean rho = 1, A_cell=1, epzilon_0=1\n",
    "    phi[ ... : ... , ... : ... ] -= ...\n",
    "    phi[ ... : ... , ... : ... ] += ...\n",
    "    \n",
    "    # Calculate maximum difference from old values\n",
    "    delta = np.max( np.abs( ... - ... ))\n",
    "    n = n+1\n",
    "\n",
    "print(\"This took \", n, \"timesteps\")\n",
    "\n",
    "# Make a plot\n",
    "plt.imshow(phi)\n",
    "plt.colorbar()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# When to use banded matrices and when to use relaxation method?\n",
    "\n",
    "- Banded matrices work on linear problems\n",
    "- Relaxation methods are slower but also work on non-linear problems!!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python [default]",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  },
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700"
   },
   "nav_menu": {
    "height": "512px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": false,
   "toc_section_display": "block",
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
