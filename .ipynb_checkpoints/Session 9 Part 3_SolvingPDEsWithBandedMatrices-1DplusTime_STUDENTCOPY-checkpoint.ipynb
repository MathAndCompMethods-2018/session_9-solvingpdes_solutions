{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Session 9 Part 3: Time dependent PDEs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib import mlab"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy import sparse\n",
    "import scipy.sparse.linalg as linalg   # linear algebra for sparse systems\n",
    "\n",
    "def createSecondOrderDiffMatrix_d2y_dx2(N, h):\n",
    "    M_SecondOrderTerm = ( -2*sparse.eye(N,N,0) + sparse.eye(N,N,1) + sparse.eye(N,N,-1) ) / h**2\n",
    "    return M_SecondOrderTerm\n",
    "    \n",
    "def createFirstOrderDiffMatrix_dy_dx(N, h):\n",
    "    M_FirstOrderTerm = ( sparse.eye(N,N,1) - sparse.eye(N,N,-1) ) / (2*h)\n",
    "    return M_FirstOrderTerm\n",
    "    \n",
    "def returnDiagonalMatrixFor_y(N):\n",
    "    M_forY = sparse.eye(N,N,0)\n",
    "    return M_forY"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "---\n",
    "\n",
    "# 5 Time dependent 1D PDEs: Timestepping\n",
    "\n",
    "## 5.1 Your first PDE: Time dependent Diffusion in 1D\n",
    "\n",
    "The example you see here is called the Method of Lines\n",
    "\n",
    "We now have evolution in 1D space and time.\n",
    "\n",
    "$$\\frac{dy}{dt}=D \\frac{d^2y}{dx^2} $$\n",
    "\n",
    "We will diffuse a step function in, for example, temperature away."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "## Initial step function which we will diffuse away below...\n",
    "\n",
    "N=100      # Specify number of nodes\n",
    "\n",
    "x = np.linspace(0,100,N)     # Discretise 1D spatial domain\n",
    "h = x[1]-x[0]                # calculate the spatial stepsize\n",
    "\n",
    "## Create an initial distribution which is a tophat function\n",
    "y=np.zeros(N)\n",
    "y[40:60]=10\n",
    "\n",
    "plt.plot(x,y)\n",
    "plt.ylim(-1,12)\n",
    "plt.ylabel(\"y\")\n",
    "plt.xlabel(\"x\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "__NOTATION:__ We will used subscripts and superscripts to denote:  $y_{space}^{time}$\n",
    "\n",
    "- The discretisaion of the RHS in space at time $t=n$ is:\n",
    "\n",
    " $$D \\frac{d^2y}{dx^2} \\approx D \\frac{y_{i+1}^n-2y_{i}^n+y_{i-1}^n}{h^2}$$\n",
    " \n",
    "- The discretisation of the LHS in time is:\n",
    "\n",
    "$$\\frac{dy}{dt} \\approx \\frac{y_{i}^{n+1} - y_{i}^n}{\\delta t}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "- Putting these together:\n",
    "\n",
    "$$\\frac{y_{i}^{n+1} - y_{i}^n}{\\delta t} = D \\frac{y_{i+1}^n-2y_{i}^n+y_{i-1}^n}{h^2}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Which we can rearrange for $y$ at the next timestep, $y_{i}^{n+1}$:\n",
    "\n",
    "$$y_{i}^{n+1} = y_{i}^n + \\delta t \\, D \\frac{  y_{i+1}^n-2y_{i}^n+y_{i-1}^n }{h^2}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "By inspection, you can see that the estimate of $y_i^{n+1}$ is expressed as a function of the the current value of $y$ and the current values at the adjacent nodes i.e. $y_{i-1}^{n}$, $y_i^{n}$ and $y_{i+1}^{n}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "This can be written as a matrix equations where:\n",
    "\n",
    "$$\\mathbf{y_{n+1}} = \\mathbf{M.y_n}$$\n",
    "\n",
    "Where: \n",
    "\n",
    "- $\\mathbf{y_n}$ are all the current values of $y$.\n",
    "- $\\mathbf{y_{n+1}}$ are all the new values of $y$.\n",
    "- $\\mathbf{M}$ is the matrix that combines together all the terms from the discretised diffusion equation above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Let's look at what needs to go into $\\mathbf{M}$ in a bit more detail.\n",
    "\n",
    " $$y_{i}^n + \\delta t \\, D \\frac{  y_{i+1}^n-2y_{i}^n+y_{i-1}^n }{h^2}$$\n",
    "\n",
    "- The first term is a term which is just the current value of $y$ which we include using an identify matrix\n",
    "- The second term is the matrix describing the second derivative, multiplied by the timestep and the diffusion constant.\n",
    "\n",
    "Let's implement this below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "## Initial step function which we will diffuse away below...\n",
    "N=100      # Specify number of nodes\n",
    "\n",
    "x = np.linspace(0,100,N)     # Discretise 1D spatial domain\n",
    "h = x[1]-x[0]                # calculate the spatial stepsize\n",
    "\n",
    "## Create an initial distribution which is a tophat function\n",
    "y=np.zeros(N)\n",
    "y[40:60]=10\n",
    "\n",
    "plt.plot(x,y)\n",
    "plt.ylim(-1,12)\n",
    "plt.ylabel(\"y\")\n",
    "plt.xlabel(\"x\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "import time\n",
    "from IPython import display\n",
    "\n",
    "## Set diffusion parameters:\n",
    "dt = 0.1   # Define timestep\n",
    "D = 2      # Define diffusion constant\n",
    "\n",
    "## Create the update matrix\n",
    "M = dt*D* createSecondOrderDiffMatrix_d2y_dx2(N, h) + returnDiagonalMatrixFor_y( N )\n",
    "\n",
    "## Loop over 200 timesteps\n",
    "for i in range(200):\n",
    "    \n",
    "    # Calculate the new y by dotting the old y with the matrix M\n",
    "    y = M.dot(y) \n",
    "    \n",
    "    # Plot the result\n",
    "    plt.plot(x,y, hold=False)\n",
    "    plt.axvline(x=x[40], color='green', linestyle=\"--\")\n",
    "    plt.axvline(x=x[60], color='green', linestyle=\"--\")\n",
    "    plt.ylim(-1,12)\n",
    "    plt.ylabel(\"y\")\n",
    "    plt.xlabel(\"x\")\n",
    "    plt.title(\"Timestep = \"+str(i))\n",
    "    display.display(plt.gcf())\n",
    "    display.clear_output(wait=True)\n",
    "    time.sleep(0.05) # necessary to get the plot up."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**QUESTION: How would I modify the code above to hold the LHS at 10 degrees?**\n",
    "- Implement this and look what happens"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 5.2 von Neumann Stability analysis of the Diffusion Equation in 1D [ADVANCED]\n",
    "\n",
    "Let's start with the diffusion equation and assume that the diffusivity, $D$ is constant.\n",
    "\n",
    "$$\\frac{\\partial u}{\\partial t} = D \\nabla^2 u$$\n",
    "\n",
    "Which in 1D discretises to,\n",
    "\n",
    "$$\\frac{u_{j}^{n+1}-u_{j}^{n}}{\\delta t} = D \\left[ \\frac{u_{j+1}^{n}-2u_{j}^{n} + u_{j-1}^{n}}{h^2} \\right] $$\n",
    "\n",
    "We need to be careful with notation here. For this 1D case, we are now using $u_j^n$ to indicate the value of $u$ at $x= j \\times h$ and $t=n \\times \\delta t$. This is not saying we are raising $u$ to the power $n$.\n",
    "\n",
    "This rearranges to,\n",
    "\n",
    "$$u_{j}^{n+1} = u_{j}^{n} + \\frac{D \\delta t}{h^2} \\left[ u_{j+1}^{n}-2u_{j}^{n} + u_{j-1}^{n} \\right] $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Basically, we need $$\\frac{D \\delta t}{h^2}$$ to be sufficiently small that the updates are small!\n",
    "\n",
    "The formal analysis below boils down to showing that:\n",
    "$$\\frac{D \\delta t}{h^2} \\le \\frac{1}{2}$$\n",
    "\n",
    "The method below shows you how to calculate this formally..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "---\n",
    "\n",
    "** Aside: von Neumann stability analysis [ADVANCED]**\n",
    "\n",
    "To analyse the stability of a finite difference scheme, we can use the von Neumann stability analysis which locally linearises the equations (if they are non-linear) and then looks for growth in the linear modes. If any modes grow - it is unstable\n",
    "\n",
    "We can use a trial solution with complex exponential to **describe oscillations in space** and an **exponential term for the time dependance**:\n",
    "\n",
    "$$u_{j,n}=A(k)^n e^{\\imath kj}$$\n",
    "\n",
    "In order to discriminate the imaginary $\\imath = \\sqrt{-1}$ from the incides $i$ we use $\\imath$ which has no dot to represent the imaginary number.\n",
    "\n",
    "Plugging this Ansatz into the finite difference equation, we can solve for $A(k)$. The equation is unstable if $|A(k)|>1$ for some $k$.\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "\n",
    "$$u_{j}^{n+1} = u_{j}^{n} + \\frac{D \\delta t}{h^2} \\left[ u_{j+1}^{n}-2u_{j}^{n} + u_{j-1}^{n} \\right] $$\n",
    "Substituting  the von Neumann ansatz into the discretised diffusion equation, we get:\n",
    "\n",
    "$$A^{n+1} e^{\\imath kj} = A^{n} e^{\\imath kj} - \\frac{D \\delta t}{h^2} \\left( A^{n} e^{\\imath k(j+1)} - 2 A^{n} e^{\\imath kj} + A^{n} e^{\\imath k(j-1)} \\right)$$\n",
    "\n",
    "Dividing through by $A^n e^{\\imath kj}$,\n",
    "\n",
    "$$A = 1 - \\frac{D \\delta t}{h^2} \\left(  e^{\\imath k} - 2 + e^{-\\imath k} \\right)$$\n",
    "\n",
    "And converting the complex exponentials to trig functions,\n",
    "\n",
    "\n",
    "$$A = 1 - \\frac{D \\delta t}{h^2} \\left(  2 \\cos k -2 \\right)\n",
    "= 1 - 4 \\frac{D \\delta t}{h^2} \\sin^2\\frac{k}{2}$$\n",
    "\n",
    "So, to ensure stability, $|A| \\le 1$,\n",
    "\n",
    "$$4 \\frac{D \\delta t}{h^2} \\le 2 \\quad \\Rightarrow  \\quad  \\frac{2 D \\delta t}{h^2} \\le 1 $$\n",
    "\n",
    "This gives a constraint on the timestep of,\n",
    "\n",
    "$$\\delta t \\le \\frac{h^2}{2 D}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "dt = 0.3   # Define timestep\n",
    "D = 2      # Define diffusion constant\n",
    "\n",
    "h = 1                # Specify the spatial stepsize\n",
    "x = np.arange(0,100,h)     # Discretise 1D spatial domain\n",
    "\n",
    "\n",
    "## Create an initial distribution which is a tophat function\n",
    "y=np.zeros_like(x)\n",
    "y[40:60]=10\n",
    "\n",
    "# Create the update matrix\n",
    "M = dt*D* createSecondOrderDiffMatrix_d2y_dx2(N, h) + returnDiagonalMatrixFor_y( N )\n",
    "\n",
    "print(\"dt \", dt)\n",
    "print(\"h^2/2D = \", h**2/(2*D))\n",
    "if(dt<h**2/(2*D)):\n",
    "    print(\"Should be stable\")\n",
    "else:\n",
    "    print(\"Solution is unstable\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import time\n",
    "from IPython import display\n",
    "\n",
    "# Loop over 200 timesteps\n",
    "for i in range(200):\n",
    "    \n",
    "    # Calculate the new y by dotting the old y with the matrix M\n",
    "    y = M.dot(y) \n",
    "    \n",
    "    # Plot the result\n",
    "    plt.plot(x,y, hold=False)\n",
    "    plt.axvline(x=x[40], color='green', linestyle=\"--\")\n",
    "    plt.axvline(x=x[60], color='green', linestyle=\"--\")\n",
    "    plt.ylim(-1,12)\n",
    "    plt.ylabel(\"y\")\n",
    "    plt.xlabel(\"x\")\n",
    "    plt.title(\"Timestep = \"+str(i))\n",
    "    display.display(plt.gcf())\n",
    "    display.clear_output(wait=True)\n",
    "    time.sleep(0.005) # necessary to get the plot up."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 5.3 The wave equation in 1D"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general, the wave equation is written:\n",
    "\n",
    "$$ \\frac{\\partial^2 \\mathbf{u}}{\\partial t}=c^2 \\nabla^2 \\mathbf{u}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "which we can write in 1D as:\n",
    "\n",
    "$$ \\frac{\\partial^2 \\mathbf{u}(x,t)}{\\partial t}=c^2 \\frac{\\partial^2 \\mathbf{u}(x,t)}{\\partial x^2} $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Discretising both sides gives:\n",
    "\n",
    "$$ \\frac{  y_{i}^{n-1}-2y_{i}^n+y_{i}^{n+1} }{\\delta t^2} = c^2 \\frac{  y_{i+1}^n-2y_{i}^n+y_{i-1}^n }{h^2}$$\n",
    "\n",
    "Rearrange this to get $y_{i}^{n+1}$ on the LHS."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Which we can rearrange to:\n",
    "\n",
    "$$  y_{i}^{n+1}  = 2y_{i}^n - y_{i}^{n-1} + \\delta t^2 \\, c^2 \\frac{  y_{i+1}^n-2y_{i}^n+y_{i-1}^n }{h^2}$$\n",
    "\n",
    "This is similar in many ways to the discretised diffusion equation for the first and last terms on the RHS.\n",
    "\n",
    "However, the estimate of the new $y$ is not just dependent on the current value BUT ALSO the previous!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Below we create a matrix to deal with: $$2y_{i}^n + \\delta t^2 \\, c^2 \\frac{  y_{i+1}^n-2y_{i}^n+y_{i-1}^n }{h^2}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "dt = 0.1   # Define timestep\n",
    "c = 0.1      # Define diffusion constant\n",
    "\n",
    "h = 0.01                # Specify the spatial stepsize\n",
    "x = np.arange(0,1.5,h)     # Discretise 1D spatial domain\n",
    "\n",
    "N = len(x)\n",
    "## Create an initial distribution which is a tophat function\n",
    "y=np.zeros_like(x)\n",
    "\n",
    "previousY = y\n",
    "\n",
    "# Create the update matrix\n",
    "M = dt**2 * c**2 * createSecondOrderDiffMatrix_d2y_dx2(N, h) + 2* returnDiagonalMatrixFor_y( N )\n",
    "\n",
    "print(dt**2 * c**2/ h**2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "We keep track of the term $$- y_{i}^{n-1}$$ inside the time loop using `currentY`.\n",
    "\n",
    "**QUESTIONS:**\n",
    "- What forcing have I applied at the LHS?\n",
    "- What BC have I applied at the RHS?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Loop over 200 timesteps\n",
    "for i in range(400):\n",
    "    currentY = y\n",
    "    # Calculate the new y by dotting the old y with the matrix M\n",
    "    y[0] = 10* np.sin(i*np.pi/40)\n",
    "    y[-1] = y[-2] \n",
    "    \n",
    "    y = M.dot(y) - previousY\n",
    "    y[0] = 10* np.sin(i*np.pi/40)\n",
    "    y[-1] = y[-2] \n",
    "    \n",
    "    previousY = currentY\n",
    "    \n",
    "    # Plot the result\n",
    "    plt.plot(x,y, hold=False)\n",
    "    plt.axvline(x=x[40], color='green', linestyle=\"--\")\n",
    "    plt.axvline(x=x[60], color='green', linestyle=\"--\")\n",
    "    plt.ylim(-12,12)\n",
    "    plt.ylabel(\"y\")\n",
    "    plt.xlabel(\"x\")\n",
    "    plt.title(\"Timestep = \"+str(i))\n",
    "    display.display(plt.gcf())\n",
    "    display.clear_output(wait=True)\n",
    "    time.sleep(0.05) # necessary to get the plot up."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## EXERCISE: How would you tackle the problems below computationally?\n",
    "\n",
    "The questions below are all taken from the Math part of this course. I want you to communiacte how you would tackle these questions but using a computational approach.\n",
    "\n",
    "$$ \\frac{\\partial T}{\\partial t}= \\kappa \\nabla^2T$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "8.3 The ends of a metal rod with thermal diffusivity $\\kappa$ are at $x = 0$ and $x = L$, and are held at a fixed temperature $T_0$. At time $t = 0$ the temperature $T(x, t)$ of the rod is given by $T(x, 0) = T_0 + T_1 \\sin \\frac{\\pi n x}{L}$, where $n$ is an integer. How does the temperature of the rod evolve? If the hottest part of the rod is at temperature $T_m(t)$, how long does it take for $T_m - T_0$ to decrease by a factor $e$ if\n",
    "\n",
    "(a) $n = 1$\n",
    "\n",
    "(b) $n = 2$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "9.1 Calculate the skin depths for the penetration of periodic daily and annual\n",
    "temperature variations into the Earth. Assume the thermal diffusivity has a value of $10^{-6}$ $m^2s^{-1}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "9.2 Assume that the temperature change associated with glaciations can be treated as a\n",
    "sinusoidal variation with period 10,000 years. Calculate the skin depth $\\delta$ for the penetration of the temperature wave into the Earth, assuming $\\kappa = 10^{-6} m^2s^{-1}$. A\n",
    "borehole is to be drilled in order to measure the terrestrial heat flow into its base.\n",
    "How deep must the hole be to reduce the temperature variations associated with the\n",
    "glaciations to 1% of their surface value?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "9.3 Calculate the temperature gradient produced at depth $Z$ in the earth by a periodic\n",
    "surface temperature variation of amplitude $\\Delta T$ and frequency $\\omega$. Compare its\n",
    "maximum value at $Z = 100$ m with the normal geothermal gradient, if the period of\n",
    "the temperature variations is 10,000 years, and the amplitude at the surface is\n",
    "$10^\\circ C$. Assume reasonable values for other parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python [default]",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  },
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700"
   },
   "nav_menu": {
    "height": "512px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": false,
   "toc_section_display": "block",
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
