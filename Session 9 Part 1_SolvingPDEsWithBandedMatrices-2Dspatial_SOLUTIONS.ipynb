{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Session 9: Banded Matrices to solve PDEs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib import mlab"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "---\n",
    "\n",
    "# 2D Partial Differential Equations\n",
    "\n",
    "Many problems in Geosciences can be represented as\n",
    "    Partial Differential Equations (PDEs).\n",
    "\n",
    "For example, the time evolving advection-diffusion of pollution away from a chimney.\n",
    "\n",
    "![Air pollution](https://breakingenergy.com/wp-content/uploads/sites/2/2013/12/181753791.jpg)\n",
    "**Figure 1: Advection-diffusion of pollution away from a chimney.**\n",
    "\n",
    "\n",
    "- In some cases analytical solutions can be found.\n",
    "- In many cases there are no analytical solutions!\n",
    "- So we need to solve numerically.\n",
    "\n",
    "Other examples include:\n",
    "\n",
    "- Oceanic and atmospheric flow for weather forecasting and climate projection,\n",
    "- Ice-sheet dynamics,\n",
    "- Mantle Convection,\n",
    "- The movement of sea-ice in the ocean.\n",
    "\n",
    "\n",
    " Aim of this session is to outline some approaches to\n",
    "    numerically modelling simple PDEs.\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "from scipy import sparse\n",
    "import scipy.sparse.linalg as linalg   # linear algebra for sparse systems\n",
    "\n",
    "def createSecondOrderDiffMatrix_d2y_dx2(N, h):\n",
    "    M_SecondOrderTerm = ( -2*sparse.eye(N,N,0) + sparse.eye(N,N,1) + sparse.eye(N,N,-1) ) / h**2\n",
    "    return M_SecondOrderTerm\n",
    "    \n",
    "def createFirstOrderDiffMatrix_dy_dx(N, h):\n",
    "    M_FirstOrderTerm = ( sparse.eye(N,N,1) - sparse.eye(N,N,-1) ) / (2*h)\n",
    "    return M_FirstOrderTerm\n",
    "    \n",
    "def returnDiagonalMatrixFor_y(N):\n",
    "    M_forY = sparse.eye(N,N,0)\n",
    "    return M_forY"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# 1. Flattening 2D data so that we can write problems as $\\mathbf{M.y}=\\mathbf{a}$\n",
    "\n",
    "When we have 2D spatial data, \n",
    "- Both $\\mathbf{y}$ and $\\mathbf{a}$ are most naturally described as rectangular arrays\n",
    "- but we know how to solve matrix equations where $\\mathbf{y}$ and $\\mathbf{a}$ are vectors\n",
    "- The answer is to flatten the data\n",
    "- Just stick all the rectangular data into a column!\n",
    "\n",
    "Let's show this with an example!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1.1 Arthur's Seat gradients \n",
    "Recall in Sesson 4 that we calculated the slopes of Arthurs seat topo data using Numpy ufuncs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dx = 2\n",
    "z = np.loadtxt('ArthursSeat.txt')\n",
    "\n",
    "# Use the central differentce approximation to the gradient\n",
    "# You wrote a function to do thin in Session 4\n",
    "dz_dx = ( z[ : , 2: ] - z[ : , :-2 ] ) / (2*dx)\n",
    "\n",
    "plt.subplot(1,2,1)\n",
    "plt.imshow(z, origin='lower', cmap='gray', extent=[0,1600,0,1600] )\n",
    "cbar = plt.colorbar(shrink=1,orientation='horizontal')\n",
    "cbar.set_label('Elevation (m)')\n",
    "cbar.set_ticks([50,100,150,200])\n",
    "plt.title('Arthurs seat elevation')\n",
    "\n",
    "plt.subplot(1,2,2)\n",
    "plt.imshow(dz_dx,origin='lower',cmap='jet',extent=[0,1600,0,1600],vmin=-.5,vmax=.5)\n",
    "cbar = plt.colorbar(shrink=1,orientation='horizontal')\n",
    "plt.xticks(range(400,2000,400))\n",
    "plt.yticks(range(400,2000,400))\n",
    "cbar.set_label('eastward slope')\n",
    "cbar.set_ticks([-.5,0,.5])\n",
    "plt.title('Arthurs seat eastward slope')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Let's now do the same thing with banded matrices so that you understand how they work in 2D. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# The shape of the elevation data array is:\n",
    "z.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# Let's flatten z and look at the new shape - it is now a single column\n",
    "z_flat=z.flatten()\n",
    "z_flat.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# The number of interior nodes on each row and column is:\n",
    "N = z.shape[0]\n",
    "N_interior = N -2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Let's construct a matrix to calculate the local slopes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# Let's create a second derivative matrix\n",
    "# This is not quite the right thing to do, but is a good start\n",
    "\n",
    "M = createFirstOrderDiffMatrix_dy_dx(N*N, dx)\n",
    "\n",
    "print(M.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "We have:\n",
    "- a vector of elevations\n",
    "- a matrix that calculates the 1st derivative (the slope...) from elevations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "**We can calculate the slope by dotting the matrix with the elevations!!**\n",
    "\n",
    "And then reshape the results back to the original shape."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "slopes_flat = M.dot(z_flat)\n",
    "\n",
    "slopes = slopes_flat.reshape( z.shape ) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.subplot(1,2,1)\n",
    "plt.imshow(slopes,origin='lower',cmap='jet',extent=[0,1600,0,1600],vmin=-.5,vmax=.5)\n",
    "cbar = plt.colorbar(shrink=1,orientation='horizontal')\n",
    "plt.xticks(range(400,2000,400))\n",
    "plt.yticks(range(400,2000,400))\n",
    "cbar.set_label('eastward slope')\n",
    "cbar.set_ticks([-.5,0,.5])\n",
    "plt.title('Arthurs seat eastward slope')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1.2 We can do better at the boundaries...\n",
    "\n",
    "Let's consider a 5x5 grid of data instead of the full 800x800 Arthur's seat dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Currently the matrix looks like this:\n",
    "\n",
    "Let's approximate all the derivatives with a central derivative:\n",
    "\n",
    "\n",
    "$$\\frac{dy_n}{dx}\\approx \\frac{y_{n-1} - 2 y_n + y_{n+1} }{2h}$$\n",
    "\n",
    "- What is the problem?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 5\n",
    "dx=1\n",
    "\n",
    "print(N)\n",
    "\n",
    "M = createFirstOrderDiffMatrix_dy_dx(N*N, dx)\n",
    "\n",
    "print(M.shape)\n",
    "\n",
    "plt.matshow( M.todense() )\n",
    "plt.colorbar()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Let's correct for the boundaries\n",
    "\n",
    "At the bondaries approximate the gradient with:\n",
    "\n",
    "A forward derivative:\n",
    "$$\\frac{dy_1}{dx}\\approx \\frac{y_2 - y_1}{h}$$\n",
    "\n",
    "Or backward derivative:\n",
    "$$\\frac{dy_N}{dx}\\approx \\frac{y_{N-1} - y_N}{h}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 5\n",
    "dx=1\n",
    "\n",
    "M = createFirstOrderDiffMatrix_dy_dx(N*N, dx)\n",
    "\n",
    "## Western BC\n",
    "for n in range(0,N*N, N):\n",
    "    M[n , n] = -1 / dx\n",
    "    M[n , n+1] = 1 / dx\n",
    "    if(n-1>-1):\n",
    "        M[n , n-1] = 0\n",
    "        \n",
    "## Eastern BC\n",
    "for n in range(N-1, N*N, N):\n",
    "    M[n , n] = 1 / dx\n",
    "    M[n , n-1] = -1 / dx\n",
    "    if(n+1<N*N):\n",
    "        M[n , n+1] = 0\n",
    "\n",
    "print(M.shape)\n",
    "\n",
    "plt.matshow( M.todense() )\n",
    "plt.colorbar()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Let's scale back up to the 800x800 Arthurs seat system.\n",
    "\n",
    "- The only difference is the matrix generation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import scipy.sparse as sp\n",
    "\n",
    "N = z.shape[0]\n",
    "dx=2\n",
    "print(N)\n",
    "\n",
    "M = createFirstOrderDiffMatrix_dy_dx(N*N, dx)\n",
    "M = sp.csr_matrix(M)\n",
    "\n",
    "for n in range(0,N*N, N):\n",
    "    # Western BC\n",
    "    M[n , n] = -1 / dx\n",
    "    M[n , n+1] = 1 / dx\n",
    "    if(n-1>-1):\n",
    "        M[n , n-1] = 0\n",
    "    \n",
    "for n in range(N-1, N*N, N):\n",
    "    # Eastern BC\n",
    "    M[n , n] = 1 / dx\n",
    "    M[n , n-1] = -1 / dx\n",
    "    if(n+1<N*N):\n",
    "        M[n , n+1] = 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And as before:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "z_flat=z.flatten()\n",
    "z_flat.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Complete the code below!\n",
    "\n",
    "slopes_flat = ... .dot( ... )\n",
    "\n",
    "slopes = slopes_flat.reshape( ... .shape ) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "## ANSWER\n",
    "\n",
    "slopes_flat = M.dot(z_flat)\n",
    "\n",
    "slopes_corr = slopes_flat.reshape( z.shape ) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "plt.subplot(1,2,1)\n",
    "plt.imshow(slopes,origin='lower',cmap='jet',extent=[0,1600,0,1600],vmin=-.5,vmax=.5)\n",
    "cbar = plt.colorbar(shrink=1,orientation='horizontal')\n",
    "plt.xticks(range(400,2000,400))\n",
    "plt.yticks(range(400,2000,400))\n",
    "cbar.set_label('eastward slope')\n",
    "cbar.set_ticks([-.5,0,.5])\n",
    "plt.title('Arthurs seat eastward slope')\n",
    "\n",
    "plt.subplot(1,2,2)\n",
    "plt.imshow(slopes-slopes_corr,origin='lower',cmap='jet',extent=[0,1600,0,1600])\n",
    "cbar = plt.colorbar(shrink=1,orientation='horizontal')\n",
    "plt.xticks(range(400,2000,400))\n",
    "plt.yticks(range(400,2000,400))\n",
    "cbar.set_label('eastward slope')\n",
    "plt.title('Slope Differences')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "The only difference is on the eastern and western boundaries:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "slopes-slopes_corr"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "---\n",
    "\n",
    "## 2 Discretising the Laplacian in 2D\n",
    "\n",
    "In 2D, the Laplacian has 2 terms,\n",
    "\n",
    "$$\\nabla^2 = \\frac{\\partial^2}{\\partial x^2}+\\frac{\\partial^2}{\\partial y^2}$$\n",
    "\n",
    "which we can discretises into separate $x$ and $y$ terms,\n",
    "\n",
    "$$ \\nabla^2_h u(x,y)=\\frac{u(x+h,y)-2u(x,y)+u(x-h,y)}{h^2}+ \\frac{u(x,y+h)-2u(x,y)+u(x,y-h)}{h^2} $$\n",
    "\n",
    "In this example we have assumed that the spatial step sizes are the same in the $x$ and $y$ directions. i.e. $dx=dy=h$.\n",
    "\n",
    "By grouping like terms, this reduces to a form that can be put into a banded matrix:\n",
    "  \n",
    "$$ h^2 \\nabla^2_h u(x,y)=u(x+h,y)+u(x,y+h)-4u(x,y)+ u(x-h,y)+u(x,y-h) $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "---\n",
    "\n",
    "## 2.1 The Laplacian as a banded matrix\n",
    "\n",
    "By grouping like terms, this reduces to a form that can be put into a banded matrix:\n",
    "\n",
    "$$ h^2 \\nabla^2_h u(x,y)=u(x+h,y)+u(x,y+h)-4u(x,y)+ u(x-h,y)+u(x,y-h) =0$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "- We can represent this visually using a 5 point stencil (Figure 3). \n",
    "\n",
    "In order to help create the sparce representation, we will 'flatten' the matrix which basically means that insteady of indexing each coordinate as an $(x,y)$ pair, we will give each cell a unique identifier.\n",
    "\n",
    "$$n_{i,j} = j \\times nx + i$$\n",
    "  \n",
    "\n",
    "<img src=\"figures/2Dstencil.jpg\" width=50%>\n",
    "\n",
    "**Figure 3: simplified grid with $nx$ points in the $x$-direction and $ny$ points in the $y$-direction. A 5 point stencil has been superimposed over point 11, to show how the adjacent cells are used to calculate the Laplacian.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "- Let's look at the terms acting cell 11 in more detail.\n",
    "\n",
    "The Laplacian in 2D for the 11th cell in the grid above can be expressed as:\n",
    "\n",
    " \\begin{align*}\n",
    "      \\nabla^2_h u[11]=\\frac{u[10]+u[12]+u[6]+u[16]-4u[11]}{h^2}\n",
    "\\end{align*}\n",
    "\n",
    "These terms have all been highlighted in the figure above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "- Let's now see how this maps onto the banded matrix\n",
    "\n",
    "Compare this equation, the figure above and the discretisation of the Laplacian in matrix form below to understand how they relate to each other. Note that the matrix is a flattened representation of the grid above.\n",
    "\n",
    "**Spend some time undertstanding how Figures 3 and 4 relate to each other!**\n",
    "\n",
    "![Flattened Matrix](figures/laplacianMatrix.png)\n",
    "**Figure 4: A table to visualise how the Laplacian illustrated in Figure 3 is mapped onto a matrix representation. Boundary nodes are coloured orange and interior nodes (those that change) are in green.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 2.2 Code to make the Laplacian as a Banded Matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "## fn to generate laplacian stencil.\n",
    "def LaplacianMatrix(nx,ny,dx,dy):\n",
    "    \"\"\" compute del_squared  on nx by ny grid. Parameters are:\n",
    "    nx -- number of points in x direction.\n",
    "    ny -- number of points in y direction.\n",
    "    dx -- separation between points in x direction.\n",
    "    dy -- separation between points in y direction.\"\"\" \n",
    "    from scipy.sparse import eye\n",
    "    \n",
    "    N = nx*ny\n",
    "    \n",
    "    ## 1. Discretise to deal with the Internal Nodes\n",
    "    dsqr = - ( 2/(dx**2) + 2/(dy**2) ) * eye(N,N,0)  # center point\n",
    "    dsqr = dsqr + ( eye(N,N,1) + eye(N,N,-1) )/dx**2    # dx\n",
    "    dsqr = dsqr + ( eye(N,N,nx) + eye(N,N,-nx) )/dy**2  # dy\n",
    "    \n",
    "    ## 2. Reset the Boundary Nodes to the identity matrix\n",
    "    index_array=np.arange(ny*nx).reshape((ny,nx)) # array of indices\n",
    "    dsqr=dsqr.tolil() \n",
    "    \n",
    "    for row in range(nx):\n",
    "        dsqr[row,row] = 1\n",
    "        dsqr[row, row-1] = 0\n",
    "        dsqr[row, row+1] = 0\n",
    "        dsqr[row, row+nx] = 0\n",
    "        dsqr[row, row-nx] = 0\n",
    "    \n",
    "    for row in range(N-nx,N):\n",
    "        dsqr[row,row] = 1\n",
    "        dsqr[row, row-1] = 0\n",
    "        if(row+1<N): \n",
    "            dsqr[row, row+1] = 0\n",
    "        if(row+nx<N): \n",
    "            dsqr[row, row+nx] = 0\n",
    "        dsqr[row, row-nx] = 0\n",
    "\n",
    "    for i in range(nx-1):\n",
    "        row = nx*(i+1)\n",
    "        dsqr[row,row] = 1\n",
    "        dsqr[row, row-1] = 0\n",
    "        if(row+1<N):\n",
    "            dsqr[row, row+1] = 0\n",
    "        if(row+nx<N): \n",
    "            dsqr[row, row+nx] = 0\n",
    "        dsqr[row, row-nx] = 0\n",
    "    \n",
    "    for i in range(nx-1):\n",
    "        row = nx*(i+1)-1\n",
    "        dsqr[row,row] = 1\n",
    "        dsqr[row, row-1] = 0\n",
    "        if(row+1<N):\n",
    "            dsqr[row, row+1] = 0\n",
    "        if(row+nx<N): \n",
    "            dsqr[row, row+nx] = 0\n",
    "        dsqr[row, row-nx] = 0\n",
    "    \n",
    "    dsqr=dsqr.tocsr() # convert back to efficient form\n",
    "    \n",
    "    return dsqr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "M_example = LaplacianMatrix(10,10,1,1)\n",
    "plt.matshow( M_example.todense() )\n",
    "plt.colorbar()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Which is the same as we presented above"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# 3. Let's now solve some probelms using banded matrices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 3.1 EXAMPLE: Solving Poisson's Equation $\\nabla^2 \\phi = 0$ using banded matrices\n",
    "\n",
    "$$ h^2 \\nabla^2_h u(x,y)=u(x+h,y)+u(x,y+h)-4u(x,y)+ u(x-h,y)+u(x,y-h) =0$$\n",
    "\n",
    "Consider the problem where we have a metal sheet where 3 sides are held at zero volts and the top edge is held at a finite voltange, $V$.\n",
    "\n",
    "In this problem, we know the voltages along all boundaries.\n",
    "\n",
    "\n",
    "<img src=\"http://www-personal.umich.edu/~mejn/cp/figures/fig9-2.png\" width=30%>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "################################################\n",
    "## Solve Poisson's Equation with a +ve voltage at the top edge.\n",
    "## Using banded matrices\n",
    "\n",
    "# Number of cells\n",
    "nx=100 ; ny=100\n",
    "\n",
    "# Create array of sources (Only along top edge)\n",
    "phi = np.zeros((nx,ny))\n",
    "phi[0,:] = 1\n",
    "\n",
    "dx=1  ;  dy=1 \n",
    "\n",
    "# Generate Laplacian matrix\n",
    "L = LaplacianMatrix(nx,ny,dx,dy) # del^2\n",
    "\n",
    "# Solve the matrices\n",
    "soln = linalg.spsolve(L, phi.flatten() )\n",
    "soln = soln.reshape((ny,nx)) # make it a 2D array rather than a large 1D array.\n",
    "\n",
    "# Plot the results\n",
    "plt.imshow(soln)\n",
    "plt.colorbar()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4.2 EXERCISE:  Solving Poisson's Equation $\\nabla^2 \\phi = -\\frac{\\rho}{\\epsilon_0}$ using the relaxation method\n",
    "\n",
    "Start with the disctretisation\n",
    "\n",
    "$$ \\nabla^2_h u(x,y)= \\frac{u(x+h,y)+u(x,y+h)-4u(x,y)+ u(x-h,y)+u(x,y-h)}{h^2} = -\\frac{\\rho(x,y)}{\\epsilon_0}$$\n",
    "\n",
    "modify the relaxation code for the Laplace equation so solve the problem where the are two square charges placed inside a 2D square box of side length 100cm. The potential is zero on all the walls and the charge densities are +1Cm$^{-1}$ and -1Cm$^{-1}$.\n",
    "\n",
    "<img src=\"http://www-personal.umich.edu/~mejn/cp/figures/fig9-4.png\" width=30%>\n",
    "\n",
    "Work in units where $\\epsilon_0=1$ and iterate until the difference between successive updates is less than $10^{-6}$V everywhere.\n",
    "\n",
    "The solution should look like:\n",
    "\n",
    "<img src=\"http://www-personal.umich.edu/~mejn/cp/figures/fig9-5.png\" width=30%>\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "################################################\n",
    "## Solve Poisson's Equation with two embeded patches of fixed charge\n",
    "## Use banded matrices\n",
    "\n",
    "# Number of cells\n",
    "nx=100 ; ny=100\n",
    "\n",
    "# Create array of sources (Only along top edge)\n",
    "phi = np.zeros((nx,ny))\n",
    "phi[... : ... , ... : ...] -= ...\n",
    "phi[... : ... , ... : ...] += ...\n",
    "\n",
    "dx=1  ;  dy=1 \n",
    "\n",
    "# Generate Laplacian matrix\n",
    "L = LaplacianMatrix( ..., ..., ..., ...) # del^2\n",
    "\n",
    "# Solve the matrices\n",
    "soln = linalg.spsolve( ... , ... )\n",
    "soln = soln.reshape( ... ) # make it a 2D array rather than a large 1D array.\n",
    "\n",
    "# Plot the results\n",
    "plt.imshow(soln)\n",
    "plt.colorbar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### ANSWER\n",
    "\n",
    "################################################\n",
    "## Solve Poisson's Equation with a +ve voltage at the top edge.\n",
    "## Using banded matrices\n",
    "\n",
    "# Number of cells\n",
    "nx=100 ; ny=100\n",
    "\n",
    "# Create array of sources (Only along top edge)\n",
    "phi = np.zeros((nx,ny))\n",
    "phi[60:80,20:40] -= 1\n",
    "phi[20:40,60:80] += 1\n",
    "\n",
    "dx=1  ;  dy=1 \n",
    "\n",
    "# Generate Laplacian matrix\n",
    "L = LaplacianMatrix(nx,ny,dx,dy) # del^2\n",
    "\n",
    "# Solve the matrices\n",
    "soln = linalg.spsolve(L, phi.flatten() )\n",
    "soln = soln.reshape((ny,nx)) # make it a 2D array rather than a large 1D array.\n",
    "\n",
    "# Plot the results\n",
    "plt.imshow(soln)\n",
    "plt.colorbar()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python [default]",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  },
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700"
   },
   "nav_menu": {
    "height": "512px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": false,
   "toc_section_display": "block",
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
